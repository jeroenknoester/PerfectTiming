import { PerfectTimingPage } from './app.po';

describe('perfect-timing App', () => {
  let page: PerfectTimingPage;

  beforeEach(() => {
    page = new PerfectTimingPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
