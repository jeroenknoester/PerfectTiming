import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Perfect Timing Application!';
  description = 'An application for visualizing your fitness using fitbit\'s data'
}
