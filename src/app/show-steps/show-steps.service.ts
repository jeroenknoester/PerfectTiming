import { Injectable }              from '@angular/core';
import { Http, Response }          from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/toPromise';

import { Steps } from './steps';

@Injectable()
export class ShowStepsService {
  // URL to web api
  private activityUrl = 'https://api.fitbit.com/1/user/-/activities/list.json?afterDate=2017-04-09&offset=0&limit=3&sort=desc';

  constructor (private http: Http) {}

  getActivities (): Promise<Steps> {
    let headers = new Headers({ 'Authorization': 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI1TE1ENkMiLCJhdWQiOiIyMjg5OUciLCJpc3MiOiJGaXRiaXQiLCJ0eXAiOiJhY2Nlc3NfdG9rZW4iLCJzY29wZXMiOiJyc29jIHJzZXQgcmFjdCBybG9jIHJ3ZWkgcmhyIHJudXQgcnBybyByc2xlIiwiZXhwIjoxNDkyNTIxMTQ0LCJpYXQiOjE0OTE5MTYzNDR9.aP44gsdkQXmlRlkN65cEVKXNmqMWZXECOMzP0tDJzng' });
    let options = new RequestOptions({ headers: headers });

    return this.http.get(this.activityUrl, options)
                    .toPromise()
                    .then(this.extractData)
                    .catch(this.handleError);
  }



  private extractData(res: Response) {
    let body = res.json();
    return body || {};
  }

  private handleError (error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Promise.reject(errMsg);
  }

}
