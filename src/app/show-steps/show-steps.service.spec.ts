import { TestBed, inject } from '@angular/core/testing';

import { ShowStepsService } from './show-steps.service';

describe('ShowStepsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ShowStepsService]
    });
  });

  it('should ...', inject([ShowStepsService], (service: ShowStepsService) => {
    expect(service).toBeTruthy();
  }));
});
