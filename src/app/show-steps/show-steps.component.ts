import { Component, OnInit } from '@angular/core';
import { ShowStepsService } from './show-steps.service';
import { Steps } from './steps';

@Component({
  selector: 'app-show-steps',
  templateUrl: './show-steps.component.html',
  styleUrls: ['./show-steps.component.css'],
  providers: [ ShowStepsService ]
})
export class ShowStepsComponent implements OnInit {
  // todo: naamgeving verbeteren.

  private activities: object = {};
  private errorMessage;

  constructor(private showStepsService: ShowStepsService) { }

  ngOnInit() {
    this.getActivities();
  }
  
  getActivities() {
    this.showStepsService.getActivities()
                    .then(
                      activities => this.activities = activities,
                      error =>  this.errorMessage = <any>error);
  }
}
