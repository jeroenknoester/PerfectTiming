import { Component, OnInit } from '@angular/core';
import { HeartrateService } from './heartrate.service';
import { HeartRate } from './heartrate';
import { ChartsModule } from 'ng2-charts';

@Component({
  selector: 'app-heartrates',
  templateUrl: './heartrates.component.html',
  styleUrls: ['./heartrates.component.css'],
  providers: [ HeartrateService ]
})

export class HeartratesComponent implements OnInit {
  private heartratelist: any[];
  private errorMessage: string;
  private valueArray: number[]; 
  private createCanvas:boolean = false;

  constructor( private heartrateService: HeartrateService ) { }

  ngOnInit() {
    this.getActivities();
  }
  
  getActivities() {
    this.heartrateService.getHeartrates()
                    .then(
                      activities => { 
                        this.heartratelist = activities,
                        this.heartratelist.forEach(elem => { this.lineChartLabels.push(elem.time)});
                        this.heartratelist.forEach(elem => { this.lineChartData[0].data.push(elem.value)});
                        this.createCanvas = true;
                        
                      },
                      error =>  this.errorMessage = <any>error);
  }

  public lineChartData:Array<any> = [
      {data: [], label: 'Heartrate'},
    ];

  public lineChartLabels:Array<any> = [];
  public lineChartOptions:any = {
    responsive: true
  };
  public lineChartColors:Array<any> = [
    { // grey
      backgroundColor: 'rgba(193, 66, 66, 0.2)',
      borderColor: 'rgba(193, 66, 66, 0.1)',
      pointBackgroundColor: 'rgba(193, 66, 66, 0.1)',
      pointBorderColor: 'red',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];
  
  public lineChartLegend:boolean = true;
  public lineChartType:string = 'line';
 
  // events
  public chartClicked(e:any):void {
    console.log(e);
  }
 
  public chartHovered(e:any):void {
    console.log(e);
  }

}
