import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeartratesComponent } from './heartrates.component';

describe('HeartratesComponent', () => {
  let component: HeartratesComponent;
  let fixture: ComponentFixture<HeartratesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeartratesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeartratesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
