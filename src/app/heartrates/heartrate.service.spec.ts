import { TestBed, inject } from '@angular/core/testing';

import { HeartrateService } from './heartrate.service';

describe('HeartrateService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HeartrateService]
    });
  });

  it('should ...', inject([HeartrateService], (service: HeartrateService) => {
    expect(service).toBeTruthy();
  }));
});
