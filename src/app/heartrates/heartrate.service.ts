import { Injectable }              from '@angular/core';
import { Http, Response }          from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { HeartRate } from './heartrate';

@Injectable()
export class HeartrateService {
  // haal locale tijd op en zet in variabele
    private hours = new Date().getHours().toString();
    private minutes = new Date().getMinutes().toString();
    private stringOfTimeNow = this.hours + ":" + this.minutes;
  // doe locale tijd min 20 min en zet in variabele
    private timeInNumbers:number = new Date().getTime();
    private twentyMinutesAgo:number = this.timeInNumbers - (70*60000);
    private time2hours = new Date(this.twentyMinutesAgo).getHours().toString();
    private time2minutes = new Date(this.twentyMinutesAgo).getMinutes().toString();
    private datetostring = this.time2hours + ":" + this.time2minutes;

  // URL to web api
  private heartrateUrl = 'https://api.fitbit.com/1/user/-/activities/heart/date/today/1d/1min/time/'+ this.datetostring +'/'+ this.stringOfTimeNow +'.json';

  constructor (private http: Http) {}

  getHeartrates (): Promise<any[]> {
    let headers = new Headers({ 'Authorization': 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI1TE1ENkMiLCJhdWQiOiIyMjg5OUciLCJpc3MiOiJGaXRiaXQiLCJ0eXAiOiJhY2Nlc3NfdG9rZW4iLCJzY29wZXMiOiJyc29jIHJzZXQgcmFjdCBybG9jIHJ3ZWkgcmhyIHJudXQgcnBybyByc2xlIiwiZXhwIjoxNDkzMTkyMjExLCJpYXQiOjE0OTI1ODc0Mzh9.ss48zu2GPLLzmNowwFu8_W2YQtqmG3ux2V1ba7JLsRU' });
    headers.append('Accept', 'application/json');
    let options = new RequestOptions({ headers: headers });

    return this.http.get(this.heartrateUrl, options)
                    .toPromise()
                    .then(this.extractData)
                    .catch(this.handleError);
  }

  private extractData(res: Response) {
    let body = res.json();
    return body['activities-heart-intraday'].dataset || {};
  }

  private handleError (error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Promise.reject(errMsg);
  }
}
